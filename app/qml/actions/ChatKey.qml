pragma Singleton
import QtQuick 2.4
import QuickFlux 1.1

KeyTable {
    property string setCurrentChat
    property string closeCurrentChat
    property string loadMoreMessages
    property string sendMessage
    property string sendChatAction
    property string leaveChat
    property string deleteChatHistory
    property string viewInDetail
    property string leaveGroupDetails
}
